# classic-flang

#### 介绍
Classic Flang is a Fortran language front-end designed for integration with LLVM.

#### 软件架构
适用于X86, Aarch64架构

#### 安装教程
1.  通过yum、dnf安装即可
```
yum install classic-flang-llvm
yum install classic-flang
```
2.  或者下载rpm包安装
```
rpm -ivh classic-flang-llvm-17.0.6-1.*.aarch64.rpm
rpm -ivh classic-flang-17.0.6-1.*.aarch64.rpm
```

#### 使用说明
1.  配置环境变量
```
# 配置classi-flang可执行文件路径
export PATH=/opt/openEuler/classic-flang-17/root/bin:$PATH
# 配置classi-flang编译时需要的动态链接库路径
export LIBRARY_PATH=/opt/openEuler/classic-flang-17/root/lib64:$LIBRARY_PATH
# 配置classi-flang编译C时头文件路径
export C_INCLUDE_PATH=/opt/openEuler/classic-flang-17/root/include:$C_INCLUDE_PATH
# 配置classi-flang编译C++时头文件路径
export CPLUS_INCLUDE_PATH=/opt/openEuler/classic-flang-17/root/include:$CPLUS_INCLUDE_PATH
# 配置运行时动态链接库路径
export LD_LIBRARY_PATH=/opt/openEuler/classic-flang-17/root/lib64:$LD_LIBRARY_PATH
```
2.  使用classic-flang编译
```
make CC=clang FC=flang F77=flang
```
或者
```
flang test.f90
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
