# classic-flang

#### Description
Classic Flang is a Fortran language front-end designed for integration with LLVM.

#### Software Architecture
Applicable to the X86 and Aarch64 architectures

#### Installation
1.  Install via yum and dnf
```
yum install classic-flang-llvm
yum install classic-flang
```
2.  Or download the rpm package and install.
```
rpm -ivh classic-flang-llvm-17.0.6-1.*.aarch64.rpm
rpm -ivh classic-flang-17.0.6-1.*.aarch64.rpm
```

#### Instructions
1.  Configuring Environment Variables
```
# Configure the executable file path of classi-flang
export PATH=/opt/openEuler/classic-flang-17/root/bin:$PATH
# Configure the dynamic library path of classi-flang
export LIBRARY_PATH=/opt/openEuler/classic-flang-17/root/lib64:$LIBRARY_PATH
# Configure the header file path when classi-flang and clang compiles C
export C_INCLUDE_PATH=/opt/openEuler/classic-flang-17/root/include:$C_INCLUDE_PATH
# Configure the header file path when classi-flang and clang compiles C++
export CPLUS_INCLUDE_PATH=/opt/openEuler/classic-flang-17/root/include:$CPLUS_INCLUDE_PATH
# Configure runtime dynamic library Path
export LD_LIBRARY_PATH=/opt/openEuler/classic-flang-17/root/lib64:$LD_LIBRARY_PATH
```
2.  Compiled with classic-flang
```
make CC=clang FC=flang F77=flang
```
or
```
flang test.f90
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
