%bcond_without sys_llvm

%global maj_ver 17
%global min_ver 0
%global patch_ver 6
%global flang_version  %{maj_ver}.%{min_ver}.%{patch_ver}
%global source_commit_id c7dc57c

%if %{with sys_llvm}
%global pkg_name classic-flang
%global bin_suffix %{nil}
%global install_prefix /opt/openEuler/%{pkg_name}-%{maj_ver}/root/
%else
%global pkg_name classic-flang%{maj_ver}
%global bin_suffix -%{maj_ver}
%global install_prefix %{_libdir}/%{pkg_name}
%endif

# note: In order to support multiple version, install dir bases on %{buildroot}/%{install_prefix}
%global install_dir_withbuildroot %{buildroot}/%{install_prefix}
%global install_bindir %{install_prefix}/bin
%global install_libdir %{install_prefix}/%{_lib}
%global install_includedir %{install_prefix}/include


# limit cpu number
%global _smp_mflags -j12
%global max_link_jobs 12

Name:    %{pkg_name}
Version: %{flang_version}
Release: 2
Summary: a Fortran language front-end designed for integration with LLVM

License: Apache-2.0 WITH LLVM-exception
URL:     https://github.com/flang-compiler/flang
Source0: https://github.com/flang-compiler/flang/archive/refs/tags/classic-flang-%{source_commit_id}.tar.gz

Patch0000: 0000-libdir-suffix.patch

BuildRequires: cmake
BuildRequires: llvm-devel >= %{version}
BuildRequires: llvm-googletest >= %{version}
BuildRequires: python3-lit >= 12.0.0
BuildRequires: clang-tools-extra >= %{version}

# The new flang drive requires clang-devel
BuildRequires: clang-devel >= %{version}
BuildRequires: clang >= %{version}
BuildRequires: clang-resource-filesystem >= %{version}
BuildRequires:  binutils-devel

%description
Flang is a ground-up implementation of a Fortran front end written in modern
C++.
 
%package devel
Summary: Flang header files
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
Flang header files.

%package llvm
Summary: llvm dependency by classic-flang

%description llvm
LLVM bin include lib files.

%prep
%autosetup -n classic-flang-%{source_commit_id} -p1

# build and install llvm-project
git clone https://gitee.com/openeuler/llvm-project.git
cd llvm-project/
cat  %{_sourcedir}/0001-llvm-package-minimize.patch | patch -p1 -F1
mkdir build
pushd build
cmake -DCMAKE_INSTALL_PREFIX=%{install_dir_withbuildroot} \
    -DLLVM_PARALLEL_LINK_JOBS=%{max_link_jobs} \
	-DCMAKE_BUILD_TYPE=RelWithDebInfo \
	-DCMAKE_C_COMPILER=gcc \
	-DCMAKE_CXX_COMPILER=g++ \
	'-DLLVM_TARGETS_TO_BUILD=ARM;AArch64;X86' \
	-DLLVM_ENABLE_CLASSIC_FLANG=on \
	-DCOMPILER_RT_BUILD_SANITIZERS=off \
	'-DLLVM_ENABLE_PROJECTS=clang;lld;compiler-rt;openmp;' \
	'-DLLVM_ENABLE_RUNTIMES=libcxx;libcxxabi;libunwind' \
	-DLLVM_USE_LINKER=gold \
	-DLLVM_LIT_ARGS="-sv %{_smp_mflags}" \
	-DLLVM_USE_SPLIT_DWARF=off \
	'-DCMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO=-Wl,--gdb-index -Wl,--compress-debug-sections=zlib' \
	'-DCMAKE_EXE_LINKER_FLAGS_DEBUG=-Wl,--gdb-index -Wl,--compress-debug-sections=zlib'   \
	-DLLVM_BINUTILS_INCDIR=/usr/include \
%if 0%{?__isa_bits} == 64
    -DLLVM_LIBDIR_SUFFIX=64            \
%else
    -DLLVM_LIBDIR_SUFFIX=              \
%endif
	-DBUILD_FOR_OPENEULER=ON \
	-DLLVM_INSTALL_TOOLCHAIN_ONLY=on \
	../llvm
make install %{_smp_mflags}
popd

%build
mkdir -p _build _install
export RPM_BUILD_DIR=%_topdir/BUILD
export SOURCE_DIR=$RPM_BUILD_DIR/classic-flang-%{source_commit_id}
export BUILD_DIR=$SOURCE_DIR/_build
export PATH=$PATH:%{install_dir_withbuildroot}/bin

# build libpgmath, and install 
mkdir -p $BUILD_DIR/libpgmath 
pushd $BUILD_DIR/libpgmath
cmake -DCMAKE_INSTALL_PREFIX=%{install_dir_withbuildroot}                 \
                -DCMAKE_BUILD_TYPE=RelWithDebInfo           \
                -DCMAKE_CXX_COMPILER=%{install_dir_withbuildroot}/bin/clang++ \
                -DCMAKE_C_COMPILER=%{install_dir_withbuildroot}/bin/clang     \
%if 0%{?__isa_bits} == 64
                -DLLVM_LIBDIR_SUFFIX=64                     \
%else
                -DLLVM_LIBDIR_SUFFIX=                       \
%endif
                $SOURCE_DIR/runtime/libpgmath
make install
popd

# build classic-flang first, and install 
mkdir -p $BUILD_DIR/flang 
pushd $BUILD_DIR/flang

cmake -DCMAKE_INSTALL_PREFIX=%{install_dir_withbuildroot}       \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo                    \
    -DCMAKE_CXX_COMPILER=%{install_dir_withbuildroot}/bin/clang++   \
    -DCMAKE_C_COMPILER=%{install_dir_withbuildroot}/bin/clang       \
    -DCMAKE_Fortran_COMPILER=flang                \
    -DCMAKE_Fortran_COMPILER_ID=Flang             \
    -DFLANG_INCLUDE_DOCS=ON                       \
    -DFLANG_LLVM_EXTENSIONS=ON                    \
    -DWITH_WERROR=ON                              \
    -DLLVM_CONFIG=%{install_dir_withbuildroot}/bin/llvm-config      \
%if 0%{?__isa_bits} == 64
    -DLLVM_LIBDIR_SUFFIX=64                       \
%else
    -DLLVM_LIBDIR_SUFFIX=                         \
%endif
    $SOURCE_DIR
    
make install
popd

%check

%files
%{install_bindir}/flang1
%{install_bindir}/flang2
%{install_libdir}/libflang.a
%{install_libdir}/libflangADT.a
%{install_libdir}/libflangArgParser.a
%{install_libdir}/libflangmain.a
%{install_libdir}/libflangrti.a
%{install_libdir}/libflangrti.so
%{install_libdir}/libflang.so
%{install_libdir}/libompstub.a
%{install_libdir}/libompstub.so
%{install_libdir}/libpgmath.a
%{install_libdir}/libpgmath.so

%files devel
%{install_libdir}/libflang.a
%{install_libdir}/libflangADT.a
%{install_libdir}/libflangArgParser.a
%{install_libdir}/libflangmain.a
%{install_libdir}/libflangrti.a
%{install_libdir}/libflangrti.so
%{install_libdir}/libflang.so
%{install_libdir}/libompstub.a
%{install_libdir}/libompstub.so
%{install_libdir}/libpgmath.a
%{install_libdir}/libpgmath.so
%{install_includedir}/ieee_arithmetic_la.mod
%{install_includedir}/ieee_arithmetic.mod
%{install_includedir}/ieee_exceptions_la.mod
%{install_includedir}/ieee_exceptions.mod
%{install_includedir}/ieee_features.mod
%{install_includedir}/iso_c_binding.mod
%{install_includedir}/iso_fortran_env.mod
%{install_includedir}/__norm2_i8.mod
%{install_includedir}/__norm2.mod
%{install_includedir}/omp_lib.h
%{install_includedir}/omp_lib_kinds.mod
%{install_includedir}/omp_lib.mod

%files llvm
%{install_prefix}/*
%exclude  %{install_prefix}/bin/clang-check
%exclude  %{install_prefix}/bin/clang-extdef-mapping
%exclude  %{install_prefix}/bin/clang-format
%exclude  %{install_prefix}/bin/clang-linker-wrapper
%exclude  %{install_prefix}/bin/clang-offload-bundler
%exclude  %{install_prefix}/bin/clang-offload-packager
%exclude  %{install_prefix}/bin/clang-refactor
%exclude  %{install_prefix}/bin/clang-rename
%exclude  %{install_prefix}/bin/clang-repl
%exclude  %{install_prefix}/bin/clang-scan-deps
%exclude  %{install_prefix}/bin/diagtool
%exclude  %{install_prefix}/bin/llvm-ar
%exclude  %{install_prefix}/bin/llvm-cov
%exclude  %{install_prefix}/bin/llvm-dwp
%exclude  %{install_prefix}/bin/llvm-ml
%exclude  %{install_prefix}/bin/llvm-nm
%exclude  %{install_prefix}/bin/llvm-objcopy
%exclude  %{install_prefix}/bin/llvm-objdump
%exclude  %{install_prefix}/bin/llvm-pdbutil
%exclude  %{install_prefix}/bin/llvm-profdata
%exclude  %{install_prefix}/bin/llvm-rc
%exclude  %{install_prefix}/bin/llvm-readobj
%exclude  %{install_prefix}/bin/llvm-size
%exclude  %{install_prefix}/bin/llvm-symbolizer

%changelog
* Mon Apr 29 2024 luofeng <luofeng13@huawei.com> - 17.0.6-2
- Add usage documents

* Fri Mar 1 2024 luofeng <luofeng13@huawei.com> - 17.0.6-1
- Enable classic flang rpm build
